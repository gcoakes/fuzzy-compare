declare module "metaphone" {
  const metaphone: (string) => string;
  export default metaphone;
}

declare module "levenshtein-edit-distance" {
  const levenshtein: (string, string) => number;
  export default levenshtein;
}
