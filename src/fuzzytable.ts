import Dexie from "dexie";
import Papa from "papaparse";
import metaphone from "metaphone";

type CsvRow = { data: Record<string, string>; metaphone: Record<string, string> };
type CsvTableIndex = { fuzzy?: string[]; index?: string[] };

class CsvDatabase extends Dexie {
  rows: Dexie.Table<CsvRow, number>;
  constructor(name: string, { fuzzy, index }: CsvTableIndex) {
    super(name);
    // Assign defaults in case they weren't specified in the callback
    // invocation.
    if (fuzzy === undefined) fuzzy = [];
    if (index === undefined) index = [];

    // Initialize the database.
    this.version(1).stores({
      rows: [
        "++",
        ...fuzzy.map((s) => "metaphone." + s),
        ...index.map((s) => "data." + s.replace(",", "")),
      ].join(","),
    });
    this.rows = this.table("rows");
  }
}

type ChooseHeadersEvent = {
  headers: string[];
  beginIndexing: (headers: CsvTableIndex) => void;
};

type IndexCsvOptions = {
  chooseHeaders: (event: ChooseHeadersEvent) => void;
  complete: (table: Dexie.Table | void) => void;
};

export function indexCsv(
  file: File,
  { chooseHeaders, complete }: IndexCsvOptions,
): void {
  if (chooseHeaders === undefined) {
    chooseHeaders = ({ headers, beginIndexing }) => beginIndexing({ fuzzy: headers });
  }
  // Static to the whole parsing process.
  let fuzzyColumns: string[] | void;
  let db: CsvDatabase | void = null;
  // Curry file name.
  const processChunk = (
    results: Papa.ParseResult<Record<string, string>>,
    parser: Papa.Parser,
  ) => {
    // Lazily initialize the database because we need to know which headers
    // exist in the database in order to ask the user which ones should be
    // metaphone indexed.
    if (db) {
      // For every row in the chunk, add entries for the metaphone of every
      // fuzzy column. Then, add it to the database.
      db.rows.bulkAdd(
        results.data.map((row) => {
          const entry = {
            metaphone: {},
            data: row,
          };
          if (fuzzyColumns) {
            for (const k of fuzzyColumns) {
              entry.metaphone[k] = metaphone(row[k]);
            }
          }
          return entry;
        }),
      );
    } else {
      // Pause the parser until we have confirmation which headers should be
      // indexed.
      parser.pause();

      // Dispatch an event to the parent so they can ask which headers should
      // be indexed.
      chooseHeaders({
        headers: results.meta.fields,
        // A callback which will initialize the database.
        beginIndexing: (indexOptions) => {
          fuzzyColumns = indexOptions.fuzzy;
          db = new CsvDatabase(file.name, indexOptions);
          db.rows.clear().then(() => {
            // Recurse because the original call to process the first chunk has
            // already gone and went.
            processChunk(results, parser);
            parser.resume();
          });
        },
      });
    }
  };
  Papa.parse(file, {
    chunk: processChunk,
    complete: () => {
      complete(db ? db.rows : null);
    },
    skipEmptyLines: true,
    header: true,
  });
}
