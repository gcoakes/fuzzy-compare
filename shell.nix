{ pkgs ? import ./nix/nixpkgs.nix { } }:
with pkgs;
let drv = import ./. { inherit pkgs; };
in mkShell {
  inputsFrom = [ drv ];
  buildInputs = [ yarn ];
}
