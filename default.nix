{ pkgs ? import ./nix/nixpkgs.nix { }, baseUrl ? "/" }:
pkgs.mkYarnPackage {
  name = "fuzzy-compare";
  packageJSON = ./package.json;
  yarnLock = ./yarn.lock;
  src = pkgs.lib.cleanSourceWith {
    filter = (path: type:
      !builtins.any (v: v == baseNameOf path) [
        "node_modules"
        "dist"
        ".cache"
      ]);
    src = pkgs.lib.cleanSource ./.;
  };

  installPhase = ''
    yarn build --public-url '${baseUrl}' -d "$out" src/index.html
  '';

  checkPhase = ''
    yarn lint
  '';

  distPhase = ":";
}
